Security and Authentication
***************************

Authentication functions for Pyramid.

.. automodule:: pyramidcms.security
    :members:
