Api
***

.. automodule:: pyramidcms.api
    :members:

api.authentication
==================

.. automodule:: pyramidcms.api.authentication
    :members:

api.authorization
=================

.. automodule:: pyramidcms.api.authorization
    :members:

api.resources
=============

.. services::
    :modules: pyramidcms.api.resources.auth
