Layouts
*******

In Pyramid, a layout class serves as the base class for class based views.

They layout class is a place to put common properties and methods that need
to be accessed by multiple view classes.

.. automodule:: pyramidcms.layouts
    :members:

BaseLayout
----------

.. automodule:: pyramidcms.layouts.base
    :members:
