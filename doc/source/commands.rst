Management Commands
*******************

This module contains all the built-in management commands.

.. automodule:: pyramidcms.commands
    :members:

collectstatic
=============

.. automodule:: pyramidcms.commands.collectstatic
    :members:

createsuperuser
===============

.. automodule:: pyramidcms.commands.createsuperuser
    :members:

dbshell
=======

.. automodule:: pyramidcms.commands.dbshell
    :members:

dumpdata
========

.. automodule:: pyramidcms.commands.dumpdata
    :members:

genkey
======

.. automodule:: pyramidcms.commands.genkey
    :members:

loaddata
========

.. automodule:: pyramidcms.commands.loaddata
    :members:

migrate
=======

.. automodule:: pyramidcms.commands.migrate
    :members:

shell
=====

.. automodule:: pyramidcms.commands.shell
    :members:
