Command Line Framework
**********************

The command line framework is used to create management commands that are
invoked using the pcms command.

.. automodule:: pyramidcms.cli
    :members:
