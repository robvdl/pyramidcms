Views
*****

This module contains class based views for the application.

.. automodule:: pyramidcms.views
    :members:

auth
----

.. automodule:: pyramidcms.views.auth
    :members:

base
----

.. automodule:: pyramidcms.views.base
    :members:

static
------

.. automodule:: pyramidcms.views.static
    :members:
