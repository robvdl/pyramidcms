import colander
from cornice.resource import resource, view
from cornice.schemas import CorniceSchema
from sqlalchemy.exc import IntegrityError
from pyramid.decorator import reify
from pyramid.httpexceptions import HTTPBadRequest, HTTPNotFound, HTTPForbidden,\
    HTTPNoContent, HTTPConflict, HTTPCreated

from pyramidcms.api.authentication import Authentication
from pyramidcms.api.authorization import ReadOnlyAuthorization
from pyramidcms.core.paginator import Paginator
from pyramidcms.core.exceptions import InvalidPage
from pyramidcms.core.messages import NOT_AUTHORIZED, NOT_AUTHENTICATED,\
    RESOURCE_NOT_FOUND, RESOURCE_EXISTS, INVALID_PAGE
from pyramidcms.security import RootFactory
from .bundle import Bundle


def get_global_acls(request):
    """
    Returns the global list of ACLs, already used by Pyramid views,
    by constructing the RootFactory object and returning __acl__.

    :param request: Pyramid request object
    :return: List of ACLs
    """
    return RootFactory(request).__acl__


def cms_resource(resource_name, **kwargs):
    """
    A helper that returns a cornice @resource decorator, pre-populating
    the collection_path, path, and name from the resource_name argument.

    :param resource_name: Name of the resource
    :return: @resource decorator
    """
    list_url = '/api/' + resource_name
    detail_url = list_url + '/{id}'

    return resource(
        name=resource_name,
        collection_path=list_url,
        path=detail_url,
        acl=get_global_acls,
        **kwargs
    )


class ApiMeta(object):
    """
    A configuration class for api resources ``BaseApi`` subclasses.

    Provides defaults and the logic needed to augment these settings with
    the internal ``class Meta`` used on ``ApiBase`` subclasses.

    Note that this is based on the ApiResource class from TastyPie,
    not everything is supported yet, this will be a gradual process.
    """
    paginator_class = Paginator
    allowed_methods = ['get', 'post', 'put', 'delete', 'patch']
    list_allowed_methods = None
    detail_allowed_methods = None
    limit = 20
    max_limit = 1000
    filtering = {}
    ordering = []
    authentication = Authentication()
    authorization = ReadOnlyAuthorization()
    object_class = None
    always_return_data = False
    schema = None

    def __new__(cls, meta=None):
        overrides = {}

        # Handle overrides.
        if meta:
            for override_name in dir(meta):
                # No internals please.
                if not override_name.startswith('_'):
                    overrides[override_name] = getattr(meta, override_name)

        methods = overrides.get('allowed_methods',
                                ['get', 'post', 'put', 'delete', 'patch'])

        if overrides.get('list_allowed_methods') is None:
            overrides['list_allowed_methods'] = methods

        if overrides.get('detail_allowed_methods') is None:
            overrides['detail_allowed_methods'] = methods

        return object.__new__(type('ApiMeta', (cls,), overrides))


class DeclarativeMetaclass(type):

    def __new__(mcs, name, bases, attrs):
        new_class = super(DeclarativeMetaclass, mcs).__new__(mcs, name, bases, attrs)
        meta = getattr(new_class, 'Meta', None)
        new_class._meta = ApiMeta(meta)

        return new_class


class ApiBase(object, metaclass=DeclarativeMetaclass):
    """
    The ``ApiBase`` class is for building RESTful resources using the
    Mozilla Cornice library.  Originally the design was modelled a bit
    after TastyPie, a RESTful API library for Django.

    There are some slight differences however, for a start we don't call the
    base class a Resource, mainly because Pyramid already has something
    called a Resource which has a completely different meaning and would
    therefore confuse Pyramid developers.

    Also, TastyPie uses offset/limit, while we use page/limit instead.
    Having offset/limit allows the front end to retrieve data starting in
    the middle of a page, however this just made the Paginator and Page
    classes more complex and just wasn't that useful, so page was used
    instead of offset.
    """

    def __init__(self, request):
        self.request = request

    @reify
    def api_url(self):
        key = [s for s in self._services if s.startswith('collection_')][0]
        return self._services[key].path

    @reify
    def resource_name(self):
        key = [s for s in self._services if not s.startswith('collection_')][0]
        return self._services[key].name

    @property
    def schema(self):
        """
        The purpose of this property is to only apply the colander schema
        for the PUT and POST request methods, which do writes.

        If we set the schema on the @resource decorator, it ends up getting
        applied to all request methods, which is not what we want.

        :return: :class:`cornice.schemas.CorniceSchema` object
        """
        if self.request.method in ('POST', 'PUT'):
            schema = self._meta.schema
        else:
            schema = None

        # if there is no schema, or the request method is GET, we still need
        # an empty Schema object, we can't just return None.
        if schema is None:
            schema = colander.Schema

        return CorniceSchema.from_colander(schema)

    def get_obj_url(self, obj_id):
        """
        Returns the API URL for the given object.
        """
        return '{}/{}'.format(self.api_url, obj_id)

    def get_obj_list(self):
        """
        The get_obj_list method must return a list of items for this
        API resource, this can be a SQLAlchemy queryset.
        """
        raise NotImplementedError

    def get_obj(self, obj_id):
        raise NotImplementedError

    def delete_obj(self, obj):
        raise NotImplementedError

    def save_obj(self, obj):
        raise NotImplementedError

    def dehydrate_obj(self, obj):
        """
        Dehydrate an object, returns a bundle.

        :param obj: the object that will be dehydrated
        :returns: :class:`pyramidcms.api.Bundle` object.
        """
        bundle = self.build_bundle(obj=obj)
        return self.dehydrate(bundle)

    def dehydrate(self, bundle):
        """
        Dehydrate a bundle.

        When implemented in API sub-classes, this method should serialize
        bundle.obj and put the resulting dict in bundle.data.

        :param bundle: :class:`pyramidcms.api.Bundle` object.
        :returns: :class:`pyramidcms.api.Bundle` object.
        """
        return bundle

    def hydrate(self, bundle):
        """
        Hydrate a bundle.

        When implemented in API sub-classes, this method should deserialize
        bundle.data and reconstruct this into an object bundle.obj.

        :param bundle: :class:`pyramidcms.api.Bundle` object.
        :returns: :class:`pyramidcms.api.Bundle` object.
        """
        return bundle

    def build_bundle(self, obj=None, data=None):
        """
        Given either an object, a data dictionary or both, builds a ``Bundle``
        for use throughout the ``dehydrate/hydrate`` cycle.
        """
        return Bundle(
            obj=obj,
            data=data,
            request=self.request,
            resource=self
        )

    @view(accept='', renderer='json')
    @view(accept='text/html', renderer='html-api')
    @view(accept='application/json', renderer='json')
    def get(self):
        """
        API endpoint for get resource (get-detail).
        """
        if self._meta.authentication.is_authenticated(self.request):
            # get the object to view
            obj_id = self.request.matchdict['id']
            obj = self.get_obj(obj_id)
            bundle = self.build_bundle(obj=obj)

            # check if we have read access to this object first
            if self._meta.authorization.read_detail(obj, bundle):
                if obj is not None:
                    bundle = self.dehydrate(bundle)
                    return bundle
                else:
                    raise HTTPNotFound(RESOURCE_NOT_FOUND.format(self.get_obj_url(obj_id)))
            else:
                raise HTTPForbidden(NOT_AUTHORIZED)
        else:
            raise HTTPForbidden(NOT_AUTHENTICATED)

    @view(accept='', renderer='json')
    @view(accept='text/html', renderer='html-api')
    @view(accept='application/json', renderer='json')
    def put(self):
        """
        API endpoint for update resource (update-detail).

        Note that TastyPie will actually create the object if it didn't
        exist first, we don't do this and return HTTPNotFound instead.
        """
        if self._meta.authentication.is_authenticated(self.request):
            # get the current object to update, build a bundle with the data
            obj_id = self.request.matchdict['id']
            obj = self.get_obj(obj_id)
            bundle = self.build_bundle(obj=obj, data=self.request.validated)

            # now we can check if we are allowed to update this object
            if self._meta.authorization.update_detail(obj, bundle):
                if obj is not None:
                    # hydrate and save the object
                    bundle = self.hydrate(bundle)

                    # if there are errors, don't call save_obj and return
                    # this results in a 400 Bad Request, which is what we want
                    if self.request.errors:
                        return

                    self.save_obj(bundle.obj)

                    # returning the data is optional and is done per-resource.
                    if self._meta.always_return_data:
                        # return the data that was saved during hydrate
                        bundle = self.dehydrate(bundle)
                        return bundle
                    else:
                        # returns 204 no content
                        return HTTPNoContent()
                else:
                    raise HTTPNotFound(RESOURCE_NOT_FOUND.format(self.get_obj_url(obj_id)))
            else:
                raise HTTPForbidden(NOT_AUTHORIZED)
        else:
            raise HTTPForbidden(NOT_AUTHENTICATED)

    @view(accept='', renderer='json')
    @view(accept='text/html', renderer='html-api')
    @view(accept='application/json', renderer='json')
    def delete(self):
        """
        API endpoint to delete a resource (delete-detail).
        """
        if self._meta.authentication.is_authenticated(self.request):
            # get the current object to delete
            obj_id = self.request.matchdict['id']
            obj = self.get_obj(obj_id)
            bundle = self.build_bundle(obj=obj)

            # now we can check if we are allowed to delete this object
            if self._meta.authorization.delete_detail(obj, bundle):
                if obj is not None:
                    # delete the object
                    self.delete_obj(obj)

                    # returns 204 no content
                    return HTTPNoContent()
                else:
                    raise HTTPNotFound(RESOURCE_NOT_FOUND.format(self.get_obj_url(obj_id)))
            else:
                raise HTTPForbidden(NOT_AUTHORIZED)
        else:
            raise HTTPForbidden(NOT_AUTHENTICATED)

    @view(accept='', renderer='json')
    @view(accept='text/html', renderer='html-api')
    @view(accept='application/json', renderer='json')
    def collection_post(self):
        """
        API endpoint for create resource (post-list).

        This needs to be a collection_post instead of post, because we are
        creating a new item, we don't need an id in the URL.
        """
        if self._meta.authentication.is_authenticated(self.request):
            # if there is an id, fetch the object so we can check if it exists.
            # if obj is None, create a new instance using _meta.object_class
            if 'id' in self.request.validated:
                obj = self.get_obj(self.request.validated['id'])
                obj_exists = obj is not None
                if not obj_exists:
                    obj = self._meta.object_class()
            else:
                obj_exists = False
                obj = self._meta.object_class()

            # create bundle based on data, obj should be None unless it exists
            bundle = self.build_bundle(obj=obj, data=self.request.validated)

            # check if we are allowed to create objects for this resource
            if self._meta.authorization.create_detail(bundle.obj, bundle):
                # we need to check if the object exists (if data has an id)
                if not obj_exists:
                    # hydrate and save the object
                    bundle = self.hydrate(bundle)

                    # if there are errors, don't call save_obj and return
                    # this results in a 400 Bad Request, which is what we want
                    if self.request.errors:
                        return

                    self.save_obj(bundle.obj)

                    # returning the data is optional and is done per-resource.
                    if self._meta.always_return_data:
                        # return the data that was saved during hydrate
                        bundle = self.dehydrate(bundle)
                        return bundle
                    else:
                        # returns 201 created
                        return HTTPCreated(location=self.get_obj_url(bundle.obj.id))
                else:
                    raise HTTPConflict(RESOURCE_EXISTS.format(self.get_obj_url(obj.id)))
            else:
                raise HTTPForbidden(NOT_AUTHORIZED)
        else:
            raise HTTPForbidden(NOT_AUTHENTICATED)

    @view(accept='', renderer='json')
    @view(accept='text/html', renderer='html-api')
    @view(accept='application/json', renderer='json')
    def collection_get(self):
        """
        API endpoint that returns a list of items for this resource (get-list).
        """
        if self._meta.authentication.is_authenticated(self.request):
            # a bundle without bundle.obj that represents the entire collection
            bundle = self.build_bundle()

            # filter results using the authorization class and create paginator
            allowed_objects = self._meta.authorization.read_list(self.get_obj_list(), bundle)
            paginator = self._meta.paginator_class(allowed_objects, self._meta.limit)
            try:
                page_number = int(self.request.GET.get('page', 1))
                page = paginator.page(page_number)
            except (ValueError, InvalidPage):
                raise HTTPBadRequest(INVALID_PAGE)

            if page.has_next():
                next_page = page.next_page_number()
                next_page_url = '{}?page={}'.format(self.api_url, next_page)
            else:
                next_page_url = None

            if page.has_previous():
                prev_page = page.previous_page_number()
                prev_page_url = '{}?page={}'.format(self.api_url, prev_page)
            else:
                prev_page_url = None

            bundle.items = [self.dehydrate_obj(obj) for obj in page.object_list]
            bundle.meta = {
                'limit': paginator.per_page,
                'next': next_page_url,
                'page': page.number,
                'num_pages': paginator.num_pages,
                'previous': prev_page_url,
                'total_count': paginator.count
            }
            return bundle
        else:
            raise HTTPForbidden(NOT_AUTHENTICATED)


class Api(ApiBase):
    """
    The regular ``Api`` class is for building API resources for non-model
    data, it is similar to the regular Resource class in TastyPie.
    """

    def get_obj_list(self):
        return []

    def get_obj(self, obj_id):
        return {}

    def delete_obj(self, obj):
        pass

    def save_obj(self, obj):
        pass


class ModelApi(ApiBase):
    """
    The ``ModelApi`` class is for building API resources for SQL Alchemy
    models, it is similar to the ModelResource class in TastyPie.
    """

    def __init__(self, request):
        super().__init__(request)

        # ApiBase class uses object_class in places, so set this to the model.
        self._meta.object_class = self._meta.model

    def get_obj_list(self):
        """
        For a ``ModelApi``, get_obj_list returns a queryset, so when we get
        to do the pagination it can apply a LIMIT to the query rather than
        fetching all the rows.
        """
        return self._meta.model.objects.all()

    def get_obj(self, obj_id):
        return self._meta.model.objects.get(id=obj_id)

    def delete_obj(self, obj):
        obj.delete()

    def save_obj(self, obj):
        try:
            obj.save(flush=True)
        except IntegrityError as ex:
            description = str(ex.orig).split('\n')[0]
            self.request.errors.add('constraint', ex.__class__.__name__, description)

    def dehydrate(self, bundle):
        """
        Dehydrate serializes the object to a dict and puts it in bundle.data

        :param bundle: :class:`pyramidcms.api.Bundle` object.
        :returns: :class:`pyramidcms.api.Bundle` object.
        """
        bundle.data = bundle.obj.serialize()
        return bundle

    def hydrate(self, bundle):
        """
        Dehydrate deserializes the bundle.data dict into bundle.obj.

        :param bundle: :class:`pyramidcms.api.Bundle` object.
        :returns: :class:`pyramidcms.api.Bundle` object.
        """
        bundle.obj.deserialize(bundle.data)
        return bundle
